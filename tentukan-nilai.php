<html>
<body> 
<?php
function tentukan_nilai($number){
    //  kode disini
    if ($number>90)
    {return "Sangat baik";}
   elseif($number<90 && $number>=75)
    {return "Baik";}
   elseif($number<75 && $number>=65)
   {return "Cukup";}
   elseif($number<65 && $number>=40)
   {return "Kurang";}
   else{ return "Error";}
}



//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
?>
</body>
</html>